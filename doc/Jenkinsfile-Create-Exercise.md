# Bitbucket / Jenkins Java Exercise

Your goal in this exercise is to get Jenkins to run the tests, build and run a docker image for the shippers demo.

The general steps are listed below.

## Step 1) You will need YOUR OWN repository with the shippers demo in it

To get YOUR OWN copy of the shippers example in a repository on bitbucket, follow these steps:

1. Login to the bitbucket website and create an **EMPTY** repository. Call it whatever you want (e.g. spring-shippers). DO NOT include a README , DO NOT include a .gitignore (we already have one in the source code). For now, ensure you make this repository public.

2. Bitbucket will suggest a command to point an existing local repository at this new empty bitbucket repository. Take a note of this command, we will use it in the next step. It will look something like:

```git remote add origin https://bitbucket.org/<your-bitbucket-name>/spring-shippers.git```

3. On your linux terminal, move into the shippers directory, re-initialise the git repository and point it at your new bitbucket repository (use the "git remote add ..." command from above)
```
cd ~/northwind-shippers-demo
git init
## note - change the below to the one suggested to you by git
git remote add origin https://bitbucket.org/<your-bitbucket-name>/spring-shippers.git
```

4. Add, Commit, Push!! (you will need to git config first):
```
git config user.email <your-email>
git config user.name "Your Name"
git add .
git commit -m "Importing the shippers-demo codebase into new repository"
git push -u origin master
```


## Step 2) Add a Jenkinsfile to your codebase

This file will tell Jenkins "what to do" with your code.

The file should be called Jenkinsfile and should be at the top of your codebase (not in a sub-directory).

You can take the example from here and edit it as needed:
https://bitbucket.org/fcallaly/northwind-shippers-demo/src/jenkinsfile/Jenkinsfile

You need to edit the first line to set a variable to a name that represents your project name (e.g. put your personal name somewhere in this name).

Try to understand the general steps that this Jenkinsfile is doing.


## Step 3) Create a Jenkins project

Create a folder in Jenkins for your work. Create a new item, select Pipeline item and give it a sensible name (perhaps the same name as the first line of the Jenkinsfile)

The only fields you'll likely want to change are:
    
    1) Give a description
    2) Check Build Triggers -> "Build when a change is pushed to Bitbucket"
    3) Select Pipeline -> "SCM"->"Git". Put your bitbucket Repository URL in the text box "Repository URL"
    4) IF you make your bitbucket project public, then you don't need to give Jenkins any credentials to access it.
    5) Save the Jenkins project



## Step 4) Test your build
On the jenkins page for your project click "Build Now". If there are errors try debugging what's happening with the "Console Output" screen.

If you get stuck, ask your instructor.

**Note:** If you're seeing an error in your build due to "archiving artifacts" then you may have to add the optional JaCoCo plugin to your project described in Step 6 below.


## Step 5) (Optional) Add a webhook to automatically trigger the build from a bitbucket push

In bitbucket, go to  "Repository Settings" -> "Webhooks" -> "Add webhook"

Set the title to Jenkins

Set the URL to point at your jenkins: http://YOUR_LINUX_HOSTNAME:8080/bitbucket-hook/

(e.g. your hostname could be like chennaidevops59.conygre.com).

Check the checkbox "Active".

Check the checkbox "Skip certificate verification".

For now only select the "Push" trigger. However, note how you can control which git actions will send this webhook.

Click 'Save'

Now try pushing a trivial change to bitbucket - e.g. you could do this by adding a README file through the bitbucket UI.

A few seconds later your build should be queued in the Jenkins UI.


## Step 6) (Optional) Ensure jacoco is added to your project.
Verify that your project uses jacoco with this plugin section in pom.xml:

	    <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>0.8.7</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>report</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
 
Check that when your project is built by Jenkins, that Jenkins saves the code coverage results.
